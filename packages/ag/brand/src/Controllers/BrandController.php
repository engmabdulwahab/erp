<?php

namespace Ag\Brand\Controllers;

use Ag\Brand\Models;
use Ag\Brand\src\Requests\EditBrandRequest;
use Ag\Brand\src\Requests\NewBrandRequest;
use Ag\Brand\src\Models\brand;
use Ag\Vendor\src\Models\Vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{

    function index() {
        return view('ag-brand::brands');
    }

    function brand($id) {

        return view('ag-brand::brand',Brand::find($id));
    }

    function createBrand() {
        return view('ag-brand::new-brand');
    }

    function editBrand(Brand $brand) {

        return view('ag-brand::edit-brand', ['brand' => $brand]);
    }

    function delBrand(Brand $brand) {
        // TODO: ECheck
        $name = $brand->name;
        if (Brand::destroy($brand->id)) {
            return redirect()->back()->with([
                'message' => 'Brand: ' . $name . ' deleted successfully!',
                'type' => 'success'
            ]);
        }

        return redirect()->back()->with([
            'message' => 'can\'t delete brand: ' . $name . '!',
            'type' => 'danger'
        ]);
    }

    function storeBrand(NewBrandRequest $request) {

        $fields = ['brand-name'];

        $atts = $request->only($fields);


        $name = $request->get('brand-name');

        $message = [
            'message' => 'Brand ' . $name . ' created successfully!',
            'type' => 'success'
        ];

        $brand = new Brand($atts);
        $save = $brand->saveOrFail();

        if ($save) {
            return redirect()->route('list-brands')
                ->with($message);
        }

        return back()->with([
            'message' => 'Failed to create brand!',
            'type' => 'danger'
        ]);
    }

    function storeEditBrand(Brand $brand, EditBrandRequest $request) {

        $request->validate([
            'brand-name' => 'required',
        ]);

        $fields   = ['brand-name'];

        $name     = $request->get('brand-name');

        $atts = $request->only($fields);

        $message = [
            'message' => 'Brand ' . $name . ' edited successfully!',
            'type' => 'success'
        ];

        $save = $brand->update($atts);

        if ($save) {
            return redirect()->route('list-brands')
                ->with($message);
        }

        return back()->with([
            'message' => 'Failed to edit brand!',
            'type' => 'danger'
        ]);
    }

    function detachVendor($id,$vendorid) {
        $brand = Brand::find($id);
        $vendor = Vendor::find($vendorid);
        $vendorName= $vendor->name;
        $vendorDet= $brand->vendors()->detach($vendor->id);
        if ($vendorDet) {
            return redirect()->back()->with([
                'message' => 'Vendor: ' . $vendorName . ' deleted successfully!',
                'type' => 'success'
            ]);
        }

        return redirect()->back()->with([
            'message' => 'can\'t delete Vendor: ' . $vendorName . '!',
            'type' => 'danger'
        ]);
    }

    function attachVendor($id) {
        $brand = Brand::find($id);
        return view('ag-brand::attachvendor',$brand);
    }

    function storeAttachVendor(Brand $brand) {
//        dd($brand->id);
//    dd(request()->all());
//        dd($id);
        $vendorid = request()->vendor_id;

        $vendor = Vendor::findOrFail($vendorid);
        $brand->vendors()->attach($vendor->id);
        $message = [
            'message' => 'Vendor ' . $vendor->name . ' edited successfully!',
            'type' => 'success'
        ];


//        if ($attach) {
            return redirect()->route('brand', $brand->id)
                ->with($message);
//        }

        dd('error');
        return back()->with([
            'message' => 'Failed to Add Vendor',
            'type' => 'danger'
        ]);
    }

    function relation() {
        $brand = Brand::first();
        //Calling
//        $vendors_id= $brand->vendors;
//        return $vendors_id;
        //Showing
//        Put this in the View
//        foreach ($vendors_id as $id){
//            echo $id->name;
//        }

        // Attaching
//        $vendor = Vendor::where('name','New Vendor')->first();
//        $brand->vendors()->attach($vendor->id);

    }


}
