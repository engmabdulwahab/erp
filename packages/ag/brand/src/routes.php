<?php

use Ag\Brand\src\Models\Brand;
use Ag\Vendor\src\Models\Vendor;
use Yajra\DataTables\Facades\DataTables;

Route::group(
    ['namespace' => 'Ag\Brand\Controllers'], function () {



    Route::group(['middleware' => ['web']], function () { // todo; Set proper
        Route::get('brands','BrandController@index')-> name('list-brands');
        Route::get('brands/new', 'BrandController@createBrand')->name('new-brand');
        Route::post('brands', 'BrandController@storeBrand')->name('new-Brand-store');
        Route::get('brands/{brand}/edit', 'BrandController@editBrand')->name('edit-brand');
        Route::post('brands/{brand}', 'BrandController@storeEditBrand')->name('edit-brand-store');
        Route::post('brands/{brand}/delete', 'BrandController@delBrand')->name('del-brand');
        Route::get('brands/{brand}/details','BrandController@brand')->name('brand');
        Route::post('brands/{brand}/details','BrandController@storeAttachVendor')->name('brand-vendor-store');
        Route::post('brands/{brand}/{vendorid}/delete','BrandController@detachVendor');
        Route::get('brands/{brand}/vendor/add','BrandController@attachVendor');


        //Testing
        Route::get('brand-vendors','BrandController@relation');

    });



//    Brand List API
    Route::name('brand-api')->get('/brand-table', function() {

        return Datatables::of(Brand::query())->make(true);
//    return User::paginate(2);

    });

//    Vendors of Brand List API
    Route::name('brand-vendors-api')->get('/brand/api/{id}', function($id) {
        $brand= Brand::query()->find($id);
        $vendors= $brand->vendors;
        return Datatables::of($vendors)->make(true);
//    return User::paginate(2);

    });

});