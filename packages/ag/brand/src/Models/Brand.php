<?php

namespace Ag\Brand\src\Models;
use Ag\Vendor\src\Models\Vendor;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    protected $fillable = [
        'brand-name'
    ];

    public function vendors() {
        return $this->belongsToMany(Vendor::class,'vendor_brand','brand_id','vendor_id');
    }
}
