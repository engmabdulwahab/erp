@extends('layouts.app')

@section('content')

    <div class="m-content">

        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-user-add"></i>
                        </span>
                        <h3 class="m-portlet__head-text m--font-brand">
                            Create new Brand
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="ag-form-submit btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span><i class="la la-save"></i> <span> Create </span> </span>
                            </a>
                        </li>
                    </ul>
                    <span class="m--margin-left-10">or <a href="{{route('list-brands')}}" class="m-link m--font-bold"> Cancel </a></span>
                </div>
            </div>
            <!--begin::Form-->
            <form id="form" method="post" action="{{ route('brand-vendor-store',\Ag\Brand\src\Models\Brand::find($id)) }}" class="m-form m-form--state m-form--fit m-form--label-align-right">
                @csrf

                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            Vendor id:
                        </label>
                        <div class="col-lg-3{{ $errors->has('id') ? ' has-danger' : ''}}">
                            <input name="vendor_id" type="text" class="form-control m-input" placeholder="Enter Brand name">
{{--                            <input name="brand_id" type="text" class="form-control m-input" placeholder="Enter Brand name" value="{{$id}}">--}}
                            <span class="form-control-feedback">
                                {{ $errors->first('id') }}
                            </span>
                            <span class="m-form__help">Please enter Vendor id</span>
                        </div>

                        {{--<div class="form-group form-md-line-input form-md-floating-label{{ $errors->has('title') ?--}}
                             {{--' has-error' : ''}}">--}}
                            {{--<input name="title" type="text" class="form-control" id="title" value="{{ old('title') }}">--}}
                            {{--<label for="title">Title</label>--}}
                            {{--<span class="help-block">Notification Title</span>--}}
                        {{--</div>--}}

                        {{--<label class="col-lg-2 col-form-label">--}}
                            {{--Contact Number:--}}
                        {{--</label>--}}
                        {{--<div class="col-lg-3">--}}
                            {{--<input type="email" class="form-control m-input" placeholder="Enter contact number">--}}
                            {{--<span class="m-form__help">--}}
               {{--Please enter user contact number--}}
               {{--</span>--}}
                        {{--</div>--}}
                    </div>

                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                    <div class="m-form__section m-form__section--last">
                        <div class="row m-form__heading">
                            <h3 class="m-form__heading-title offset-lg-1">
                                Brand Info:
                                <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Credentials to login to the system"></i>
                            </h3>
                        </div>

                    </div>


                </div>

                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6 m--align-right">
                                <button type="button" class="ag-form-submit btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span><i class="la la-save"></i> <span> Create </span> </span>
                                </button>

                                <span class="m--margin-left-10">or <a href="{{route('list-brands')}}" class="m-link m--font-bold"> Cancel </a></span>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->


    </div>
@endsection

@section('scripts')
    @parent

    <script>
        $("#form").validate({
            rules: {

            }
        });

        $('.ag-form-submit').click(function(e) {
            e.preventDefault()
            $('#form').submit();
        });
        $(window).bind('beforeunload', function(e){
            // TODO: implelemt logic
            // return null
            // if($('#form').serialize()!=$('#form').data('serialize'))return true;
            // else e=null;
            // e=null
        });
    </script>
@endsection
