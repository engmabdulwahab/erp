<?php

use Ag\Customer\src\Models\customer;
use Yajra\DataTables\Facades\DataTables;

Route::group(
    ['namespace' => 'Ag\Customer\Controllers'], function () {



    Route::group(['middleware' => ['web']], function () { // todo; Set proper
        Route::get('customers','CustomerController@index')-> name('list-customers');
        Route::get('customers/new', 'CustomerController@createCustomer')->name('new-customer');
        Route::post('customers', 'CustomerController@storeCustomer')->name('new-Customer-store');
        Route::get('customers/{customer}/edit', 'CustomerController@editCustomer')->name('edit-customer');
        Route::post('customers/{customer}', 'CustomerController@storeEditCustomer')->name('edit-customer-store');
        Route::post('customers/{customer}/delete', 'CustomerController@delCustomer')->name('del-customer');

    });



//    Customer List API
    Route::name('demo1-api')->get('/demo-table', function() {

        return Datatables::of(customer::query())->make(true);
//    return User::paginate(2);

    });



    });