<?php

namespace Ag\Customer;

use Illuminate\Support\ServiceProvider;

class CustomerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';


        $this->loadViewsFrom(__DIR__.'/views', 'ag-customer');


        // Migrations
        $this->loadMigrationsFrom([
            __DIR__ . '/../database/migrations'
        ]);
    }
}
