<?php
/**
 * Created by PhpStorm.
 * User: agile
 * Date: 2/22/19
 * Time: 3:54 PM
 */

namespace Ag\customer\src\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:191',
            'phone'     => 'required|starts_with:971|digits_between:11,13',
        ];
    }

//    public function messages()
//    {
//        return [
//            'title.required' => 'Notification title is required!',
//            'message.required' => 'Notification text is required!',
//            'cat.in' => 'Selected type is wrong. It may be technical issue!',
//        ];
//    }

}