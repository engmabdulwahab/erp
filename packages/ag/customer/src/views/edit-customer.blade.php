@extends('layouts.app')

@section('content')

    <div class="m-content">

        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-user-add"></i>
                        </span>
                        <h3 class="m-portlet__head-text m--font-brand">
                            Create new Customer
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="ag-form-submit btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span><i class="la la-save"></i> <span> Save </span> </span>
                            </a>
                        </li>
                    </ul>
                    <span class="m--margin-left-10">or <a href="{{route('list-customers')}}" class="m-link m--font-bold"> Cancel </a></span>
                </div>
            </div>
            <!--begin::Form-->
            <form id="form" method="post" action="{{ route('edit-customer-store', $customer) }}" class="m-form m-form--state m-form--fit m-form--label-align-right">
                @csrf

                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            Full Name:
                        </label>
                        <div class="col-lg-3{{ $errors->has('name') ? ' has-danger' : ''}}">
                            <input name="name" type="text" class="form-control m-input" placeholder="Enter full name" value="{{ old('name', $customer->name) }}">
                            <span class="form-control-feedback">
                                {{ $errors->first('name') }}
                            </span>
                            <span class="m-form__help">Please enter user full name</span>
                        </div>

                    </div>

                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                    <div class="m-form__section m-form__section--last">
                        <div class="row m-form__heading">
                            <h3 class="m-form__heading-title offset-lg-1">
                                Customer Info:
                                <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Credentials to login to the system"></i>
                            </h3>
                        </div>

                        <div class="form-group m-form__group row">

                                <label class="col-lg-2 col-form-label">
                                    Phone:
                                </label>
                                <div class="col-lg-3{{ $errors->has('phone') ? ' has-danger' : ''}}">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input name="phone" type="text" class="form-control m-input"
                                               value="{{ old('phone', $customer->phone) }}" placeholder="Enter Customer Phone Number">
                                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                          <span>
                                          <i class="la la-phone"></i>
                                          </span>
                                      </span>
                                    </div>

                                    <span class="form-control-feedback">
                                        {{ $errors->first('phone') }}
                                    </span>
                                    <span class="m-form__help">
                                       Please enter customer phone number
                                    </span>
                                </div>


                        </div>

                    </div>


                </div>

                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6 m--align-right">
                                <button type="button" class="ag-form-submit btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span><i class="la la-save"></i> <span> Save </span> </span>
                                </button>

                                <span class="m--margin-left-10">or <a href="{{route('list-customers')}}" class="m-link m--font-bold"> Cancel </a></span>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->


    </div>
@endsection

@section('scripts')
    @parent

    <script>
        $("#form").validate({
            rules: {
                email: {
                    required: !0,
                    email: !0
                },
                name: {
                    required: !0
                },
                password: {
                    minlength: 6
                }
            }
        });

        $('.ag-form-submit').click(function(e) {
            e.preventDefault()
            $('#form').submit();
        });
        $(window).bind('beforeunload', function(e){
            // TODO: implelemt logic
            // return null
            // if($('#form').serialize()!=$('#form').data('serialize'))return true;
            // else e=null;
            // e=null
        });
    </script>
@endsection
