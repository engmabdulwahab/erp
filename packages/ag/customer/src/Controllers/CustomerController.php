<?php

namespace Ag\Customer\Controllers;

use Ag\Customer\Models;
use Ag\customer\src\Requests\EditCustomerRequest;
use Ag\customer\src\Requests\NewCustomerRequest;
use Ag\Customer\src\Models\customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{

    function index() {
        return view('ag-customer::customers');
    }


    function createCustomer() {
        return view('ag-customer::new-customer');
    }

    function editCustomer(Customer $customer) {

        return view('ag-customer::edit-customer', ['customer' => $customer]);
    }

    function delCustomer(Customer $customer) {
        // TODO: ECheck
        $name = $customer->name;
        if (Customer::destroy($customer->id)) {
            return redirect()->back()->with([
                'message' => 'Customer: ' . $name . ' deleted successfully!',
                'type' => 'success'
            ]);
        }

        return redirect()->back()->with([
            'message' => 'can\'t delete customer: ' . $name . '!',
            'type' => 'danger'
        ]);
    }

    function storeCustomer(NewCustomerRequest $request) {

        $fields = ['name', 'phone'];

        $atts = $request->only($fields);


        $name = $request->get('name');

        $message = [
            'message' => 'Customer ' . $name . ' created successfully!',
            'type' => 'success'
        ];

        $customer = new Customer($atts);
        $save = $customer->saveOrFail();

        if ($save) {
            return redirect()->route('list-customers')
                ->with($message);
        }

        return back()->with([
            'message' => 'Failed to create customer!',
            'type' => 'danger'
        ]);
    }

    function storeEditCustomer(Customer $customer, EditCustomerRequest $request) {

        $request->validate([
            'phone' => 'required',
        ]);

        $fields   = ['name', 'phone'];

        $name     = $request->get('name');
        $phone = $request->get('phone');

        $atts = $request->only($fields);


        $message = [
            'message' => 'Customer ' . $name . ' edited successfully!',
            'type' => 'success'
        ];

        $save = $customer->update($atts);

        if ($save) {
            return redirect()->route('list-customers')
                ->with($message);
        }

        return back()->with([
            'message' => 'Failed to edit customer!',
            'type' => 'danger'
        ]);
    }


}
