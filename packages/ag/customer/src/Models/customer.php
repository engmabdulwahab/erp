<?php

namespace Ag\Customer\src\Models;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    //
    protected $fillable = [
        'name','phone',
    ];
}
