<?php

namespace Ag\Vendor\src\Models;

use Ag\Brand\src\Models\Brand;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Vendor extends Model
{
    //
    protected $fillable = [
        'name'
    ];

    use HasRoles;

    protected $guard_name = 'web';

    public function brands() {
        return $this->belongsToMany(Brand::class,'vendor_brand','vendor_id','brand_id');
    }
}
