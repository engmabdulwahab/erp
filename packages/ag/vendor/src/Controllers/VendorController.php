<?php

namespace Ag\Vendor\Controllers;

use Ag\Vendor\Models;
use Ag\vendor\src\Requests\EditVendorRequest;
use Ag\vendor\src\Requests\NewVendorRequest;
use Ag\Vendor\src\Models\vendor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorController extends Controller
{

    public function __construct() // check if user has logged / or have the right permission/role
    {
        $this->middleware(['auth']);
        $this->middleware(['permission:edit vendors'])->only('editVendor');

    }

    function index() {
        return view('ag-vendor::vendors');
    }


    function createVendor() {
        return view('ag-vendor::new-vendor');
    }

    function editVendor(Vendor $vendor) {

        return view('ag-vendor::edit-vendor', ['vendor' => $vendor]);
    }

    function delVendor(Vendor $vendor) {
        // TODO: ECheck
        $name = $vendor->name;
        if (Vendor::destroy($vendor->id)) {
            return redirect()->back()->with([
                'message' => 'Vendor: ' . $name . ' deleted successfully!',
                'type' => 'success'
            ]);
        }

        return redirect()->back()->with([
            'message' => 'can\'t delete vendor: ' . $name . '!',
            'type' => 'danger'
        ]);
    }

    function storeVendor(NewVendorRequest $request) {

        $fields = ['name'];

        $atts = $request->only($fields);


        $name = $request->get('name');

        $message = [
            'message' => 'Vendor ' . $name . ' created successfully!',
            'type' => 'success'
        ];

        $vendor = new Vendor($atts);
        $save = $vendor->saveOrFail();

        if ($save) {
            return redirect()->route('list-vendors')
                ->with($message);
        }

        return back()->with([
            'message' => 'Failed to create vendor!',
            'type' => 'danger'
        ]);
    }

    function storeEditVendor(Vendor $vendor, EditVendorRequest $request) {

        $request->validate([
            'name' => 'required',
        ]);

        $fields   = ['name'];

        $name     = $request->get('name');

        $atts = $request->only($fields);


        $message = [
            'message' => 'Vendor ' . $name . ' edited successfully!',
            'type' => 'success'
        ];

        $save = $vendor->update($atts);

        if ($save) {
            return redirect()->route('list-vendors')
                ->with($message);
        }

        return back()->with([
            'message' => 'Failed to edit vendor!',
            'type' => 'danger'
        ]);
    }


}
