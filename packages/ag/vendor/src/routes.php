<?php

use Ag\Vendor\src\Models\vendor;
use Yajra\DataTables\Facades\DataTables;

Route::group(
    ['namespace' => 'Ag\Vendor\Controllers'], function () {



    Route::group(['middleware' => ['web']], function () { // todo; Set proper
        Route::get('vendors','VendorController@index')-> name('list-vendors');
        Route::get('vendors/new', 'VendorController@createVendor')->name('new-vendor');
        Route::post('vendors', 'VendorController@storeVendor')->name('new-Vendor-store');

        Route::post('vendors/{vendor}', 'VendorController@storeEditVendor')->name('edit-vendor-store');
        Route::post('vendors/{vendor}/delete', 'VendorController@delVendor')->name('del-vendor');
        Route::get('vendors/{vendor}/edit', 'VendorController@editVendor')->name('edit-vendor');

    });



//    Vendor List API
    Route::name('vendor-api')->get('/vendor-table', function() {

        return Datatables::of(vendor::query())->make(true);
//    return User::paginate(2);

    });



});