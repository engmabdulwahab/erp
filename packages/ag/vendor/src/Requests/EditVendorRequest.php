<?php
/**
 * Created by PhpStorm.
 * User: agile
 * Date: 2/22/19
 * Time: 3:54 PM
 */

namespace Ag\Vendor\src\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditVendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:191',
        ];
    }

}