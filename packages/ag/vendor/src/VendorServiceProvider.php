<?php

namespace Ag\Vendor;

use Illuminate\Support\ServiceProvider;

class VendorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';

       // Load Views
        $this->loadViewsFrom(__DIR__.'/views', 'ag-vendor');


        // Migrations
        $this->loadMigrationsFrom([
            __DIR__ . '/../database/migrations'
        ]);
    }
}
