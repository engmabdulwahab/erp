@extends('layouts.app')

@section('content')

    <div class="m-content">

        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-user-add"></i>
                        </span>
                        <h3 class="m-portlet__head-text m--font-brand">
                            <span>Edit <u>{{$role->{'name'} }}</u> Role Name</span>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="ag-form-submit btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span><i class="la la-save"></i> <span> Save </span> </span>
                            </a>
                        </li>
                    </ul>
                    <span class="m--margin-left-10">or <a href="{{route('list-roles')}}" class="m-link m--font-bold"> Cancel </a></span>
                </div>
            </div>
            <!--begin::Form-->
            <form id="form" method="post" action="{{ route('edit-role', $role) }}" class="m-form m-form--state m-form--fit m-form--label-align-right">
                @csrf

                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            Role Name:
                        </label>
                        <div class="col-lg-3{{ $errors->has('name') ? ' has-danger' : ''}}">
                            <input name="name" type="text" class="form-control m-input" placeholder="Enter Role name" value="{{ old('name', $role->{'name'} ) }}">
                            <span class="form-control-feedback">
                                {{ $errors->first('name') }}
                            </span>
                            <span class="m-form__help">Please enter Role name</span>
                        </div>

                    </div>


                </div>



            </form>
            <!--end::Form-->
            @if ($permissions->count())
                <div class="m-form__seperator m-form__seperator--dashed"></div>
                <div class="m-form__section m-form__section--last">
                    <div class="row m-form__heading">
                        <h3 class="m-form__heading-title offset-lg-1">
                            Role Permissions:
                            <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Credentials to login to the system"></i>
                        </h3>

                        <div class="box">
                            @foreach($permissions as $permission)

                                <form name="permissions" method="POST" action="{{url('roles/'.$role->id.'/'.$permission->id)}}" class="m-form m-form--state m-form--fit m-form--label-align-right">
                                    @csrf
                                    @method('PATCH')
{{--                                    onchange="this.form.submit()--}}
                                    <div class="form-group m-form__group row">
                                        <label for="permissions" class="checkbox">
                                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" @if($role->permissions->contains($permission)) checked @endif onchange="mySubmit(this.form)">
                                            {{--                                                <input type="checkbox" name="" onchange="this.form.submit()" {{$permission ? 'checked' : ''}}>--}}
                                            {{--                                                <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" checked>--}}
                                            {{$permission->name}}
                                        </label>

                                    </div>
                                </form>
                            @endforeach
                        </div>


                    </div>

                </div>

            @endif


{{--            testing ajax--}}
{{--          TODO: Refactoring--}}

            <script>
                function mySubmit(theForm) {
                    $.ajax({ // create an AJAX call...
                        data: $(theForm).serialize(), // get the form data
                        type: $(theForm).attr('method'), // GET or POST
                        url: $(theForm).attr('action'), // the file to call
                        success: function (response) { // on success..
                            $('#here').html(response); // update the DIV

                        }
                    });
                }
            </script>

            <div id="here"></div>
        </div>
        <!--end::Portlet-->


    </div>
@endsection

@section('scripts')
    @parent

    <script>
        $("#form").validate({
            rules: {
                email: {
                    required: !0,
                    email: !0
                },
                name: {
                    required: !0
                },
                password: {
                    minlength: 6
                }
            }
        });

        $('.ag-form-submit').click(function(e) {
            e.preventDefault()
            $('#form').submit();
        });
        $(window).bind('beforeunload', function(e){
            // TODO: implelemt logic
            // return null
            // if($('#form').serialize()!=$('#form').data('serialize'))return true;
            // else e=null;
            // e=null
        });
    </script>
@endsection
