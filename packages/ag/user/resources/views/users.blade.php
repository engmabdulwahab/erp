@extends('layouts.app')

@section('content')
    <!-- TODO: Is breadcrum imp ?! -->
{{--<div class="m-subheader ">--}}
    {{--<div class="d-flex align-items-center">--}}
        {{--<div class="mr-auto">--}}
            {{--<h3 class="m-subheader__title m-subheader__title--separator">--}}
                {{--Users--}}
            {{--</h3>--}}
            {{--<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">--}}
                {{--<li class="m-nav__item m-nav__item--home">--}}
                    {{--<a href="#" class="m-nav__link m-nav__link--icon">--}}
                        {{--<i class="m-nav__link-icon la la-home"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="m-nav__separator">--}}
                    {{-----}}
                {{--</li>--}}
                {{--<li class="m-nav__item">--}}
                    {{--<a href="" class="m-nav__link">--}}
																			{{--<span class="m-nav__link-text">--}}
																				{{--Metronic Datatable--}}
																			{{--</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="m-nav__separator">--}}
                    {{-----}}
                {{--</li>--}}
                {{--<li class="m-nav__item">--}}
                    {{--<a href="" class="m-nav__link">--}}
																			{{--<span class="m-nav__link-text">--}}
																				{{--Base--}}
																			{{--</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="m-nav__separator">--}}
                    {{-----}}
                {{--</li>--}}
                {{--<li class="m-nav__item">--}}
                    {{--<a href="" class="m-nav__link">--}}
																			{{--<span class="m-nav__link-text">--}}
																				{{--JSON Data--}}
																			{{--</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        {{--<div>--}}
            {{--<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">--}}
                {{--<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">--}}
                    {{--<i class="la la-plus m--hide"></i>--}}
                    {{--<i class="la la-ellipsis-h"></i>--}}
                {{--</a>--}}
                {{--<div class="m-dropdown__wrapper">--}}
                    {{--<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>--}}
                    {{--<div class="m-dropdown__inner">--}}
                        {{--<div class="m-dropdown__body">--}}
                            {{--<div class="m-dropdown__content">--}}
                                {{--<ul class="m-nav">--}}
                                    {{--<li class="m-nav__section m-nav__section--first m--hide">--}}
																							{{--<span class="m-nav__section-text">--}}
																								{{--Quick Actions--}}
																							{{--</span>--}}
                                    {{--</li>--}}
                                    {{--<li class="m-nav__item">--}}
                                        {{--<a href="" class="m-nav__link">--}}
                                            {{--<i class="m-nav__link-icon flaticon-share"></i>--}}
                                            {{--<span class="m-nav__link-text">--}}
																									{{--Activity--}}
																								{{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="m-nav__item">--}}
                                        {{--<a href="" class="m-nav__link">--}}
                                            {{--<i class="m-nav__link-icon flaticon-chat-1"></i>--}}
                                            {{--<span class="m-nav__link-text">--}}
																									{{--Messages--}}
																								{{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="m-nav__item">--}}
                                        {{--<a href="" class="m-nav__link">--}}
                                            {{--<i class="m-nav__link-icon flaticon-info"></i>--}}
                                            {{--<span class="m-nav__link-text">--}}
																									{{--FAQ--}}
																								{{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="m-nav__item">--}}
                                        {{--<a href="" class="m-nav__link">--}}
                                            {{--<i class="m-nav__link-icon flaticon-lifebuoy"></i>--}}
                                            {{--<span class="m-nav__link-text">--}}
																									{{--Support--}}
																								{{--</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="m-nav__separator m-nav__separator--fit"></li>--}}
                                    {{--<li class="m-nav__item">--}}
                                        {{--<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">--}}
                                            {{--Submit--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-users"></i>
                        </span>
                        <h3 class="m-portlet__head-text m--font-brand">
                            Users List
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('new-user') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                            <span>
                                <i class="la la-plus"></i>
                                <span>
                                    New User
                                </span>
                            </span>
                            </a>
                        </li>
                        <li class="m-portlet__nav-item"></li>
                        <li class="m-portlet__nav-item">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                    <i class="la la-ellipsis-h m--font-brand"></i>
                                </a>
                                <div class="m-dropdown__wrapper" style="z-index: 101;">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 21.5px;"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <li class="m-nav__section m-nav__section--first">
                                                    <span class="m-nav__section-text">
																											Quick Actions
																										</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-share"></i>
                                                            <span class="m-nav__link-text">
																												Create Post
																											</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                            <span class="m-nav__link-text">
																												Send Messages
																											</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-multimedia-2"></i>
                                                            <span class="m-nav__link-text">
																												Upload File
																											</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__section">
                                                    <span class="m-nav__section-text">
																											Useful Links
																										</span>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-info"></i>
                                                            <span class="m-nav__link-text">
																												FAQ
																											</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                            <span class="m-nav__link-text">
																												Support
																											</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__separator m-nav__separator--fit m--hide"></li>
                                                    <li class="m-nav__item m--hide">
                                                        <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
                                                            Submit
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">

                @if (session('message'))
                    <div class="alert alert-{{ session('type', 'brand') }} alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        {{ session('message') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <div class="m-form__group m-form__group--inline">
                                        <div class="m-form__label">
                                            <label>Status:</label>
                                        </div>
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select" id="m_form_status">
                                                <option value="">All</option>
                                                <option value="1">Active</option>
                                                <option value="0">Not active</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                                <div class="col-md-4">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
									<span><i class="la la-search"></i></span>
								</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->

                <!--begin: Datatable -->
                <div class="m_datatable" id="ajax_data"></div>
                <!--end: Datatable -->
            </div>

        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>

@endsection



@section('scripts')
    @parent
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

    <script>
        var AGDatatable = {
            delAlert: function(id,name) {
                swal({
                    title: "Are you sure you want to delete "+ name +"?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: !0,
                    confirmButtonText: "Yes, delete it!"
                }).then(function(e) {
                    // todo: alax delete

                    e.value && document.getElementById('user-'+id).submit()
                })
            },

            init: function() {
                var t;
                t = $(".m_datatable").mDatatable({
                    data: {
                        type: "remote",
                        source: {
                            read: {
                                url: "{!! route('demo-api') !!}",
                                method: 'GET'
                            }
                        },
                        pageSize: 10,
                        serverPaging: !0,
                        serverFiltering: !1,
                        serverSorting: !1
                    },
                    layout: {
                        theme: "default",
                        class: "",
                        scroll: !1,
                        height: null,
                        footer: !1
                    },
                    sortable: !0,
                    pagination: !0,
                    toolbar: {
                        items: {
                            pagination: {
                                pageSizeSelect: [10, 20, 30, 50, 100]
                            }
                        }
                    },
                    search: {
                        input: $("#generalSearch")
                    },
                    columns: [{
                        field: "id",
                        title: "#",
                        sortable: !1,
                        textAlign: "center",
                        width: 40,
                        selector: {
                            class: "m-checkbox--solid m-checkbox--brand"
                        }
                    }, {
                        field: "name",
                        title: "Name",
                        sortable: "asc",
                        filterable: !1,
                        width: 150
                    }, {
                        field: "email",
                        title: "Email",
                        filterable: !1,
                        width: 150
                    },
                    {
                        field: "created_at",
                        title: "Creation Date",
                        type: "date",
                        textAlign:"center",
                        format: "DD/MM/YYYY" // number
                    },
                    {
                        field: "updated_at",
                        title: "Last Update",
                        type: "date",
                        textAlign:"center",
                        format: "DD/MM/YYYY" // number
                    }, {
                            field: "status",
                            title: "Status",
                            width: 90,
                            textAlign:"center",
                            template: function(t) {
                                var st = t.status
                                if (t.status != 1 && t.status != 0) {
                                    st = 2
                                }

                                var e = {
                                    2: {
                                        title: "Not valid!",
                                        class: "m-badge--danger"
                                    },
                                    1: {
                                        title: "Active",
                                        class: "m-badge--success"
                                    },
                                    0: {
                                        title: "Inactive",
                                        class: " m-badge--warning"
                                    }
                                };
                                return '<span class="m-badge ' + e[st].class + ' m-badge--wide">' + e[st].title + "</span>"
                            }
                        }, {
                            field: "Actions",
                            width: 100,
                            title: "Actions",
                            sortable: !1,
                            textAlign:"center",
                            overflow: "visible",
                            template: function(t, e, a) {


                                // return
                                //     '\t\t\t\t\t\t<div class="dropdown ' + (a.getPageSize() - e <= 4 ? "dropup" : "") + '">\t\t\t\t\t\t\t' +
                                //     '<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"> <i class="la la-ellipsis-h"></i> </a>\t\t\t\t\t\t  \t' +
                                //     '<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t    \t' +
                                //     '<a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\t\t\t\t\t\t    \t' +
                                //     '<a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\t\t\t\t\t\t    \t' +
                                //     '<a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t' +
                                   return '<a href="users/'+t.id+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\t\t\t\t\t\t\t<i class="la la-edit"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t' +
                                    '<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"' +
                                       'onclick="event.preventDefault();AGDatatable.delAlert('+t.id+', \''+t.name+'\');" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t</a>\n' +
                                       '<form id="user-'+t.id+'" action="users/'+t.id+'/delete" method="POST" style="display: none;">@csrf</form>'
                            }
                        }]
                }), $("#m_form_status").on("change", function() {
                    t.search($(this).val(), "status")
                }), $("#m_form_type").on("change", function() {
                    t.search($(this).val(), "Type")
                }), $("#m_form_status, #m_form_type").selectpicker()

            }
        };
        jQuery(document).ready(function() {
            AGDatatable.init()
        });

    </script>
@endsection
