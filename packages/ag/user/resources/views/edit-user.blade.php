@extends('layouts.app')

@section('content')

    <div class="m-content">

        <!--begin::Portlet-->
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-user-add"></i>
                        </span>
                        <h3 class="m-portlet__head-text m--font-brand">
                            Create new user
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" class="ag-form-submit btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span><i class="la la-save"></i> <span> Save </span> </span>
                            </a>
                        </li>
                    </ul>
                    <span class="m--margin-left-10">or <a href="{{route('list-users')}}" class="m-link m--font-bold"> Cancel </a></span>
                </div>
            </div>
            <!--begin::Form-->
            <form id="form" method="post" action="{{ route('edit-user-store', $user) }}" class="m-form m-form--state m-form--fit m-form--label-align-right">
                @csrf

                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-form-label">
                            Full Name:
                        </label>
                        <div class="col-lg-3{{ $errors->has('name') ? ' has-danger' : ''}}">
                            <input name="name" type="text" class="form-control m-input" placeholder="Enter full name" value="{{ old('name', $user->name) }}">
                            <span class="form-control-feedback">
                                {{ $errors->first('name') }}
                            </span>
                            <span class="m-form__help">Please enter user full name</span>
                        </div>

                    </div>

                    <div class="m-form__seperator m-form__seperator--dashed"></div>
                    <div class="m-form__section m-form__section--last">
                        <div class="row m-form__heading">
                            <h3 class="m-form__heading-title offset-lg-1">
                                Login Info:
                                <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info" title="Credentials to login to the system"></i>
                            </h3>
                        </div>

                        <div class="form-group m-form__group row">

                                <label class="col-lg-2 col-form-label">
                                    Email:
                                </label>
                                <div class="col-lg-3{{ $errors->has('email') ? ' has-danger' : ''}}">
                                    <div class="m-input-icon m-input-icon--right">
                                        <input name="email" type="email" class="form-control m-input"
                                               value="{{ old('email', $user->email) }}" placeholder="Enter user email address">
                                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                          <span>
                                          <i class="la la-envelope"></i>
                                          </span>
                                      </span>
                                    </div>

                                    <span class="form-control-feedback">
                                        {{ $errors->first('email') }}
                                    </span>
                                    <span class="m-form__help">
                                       Please enter user email address
                                    </span>
                                </div>

                            <label class="col-lg-2 col-form-label">
                                Password:
                            </label>
                            <div class="col-lg-3{{ $errors->has('password') ? ' has-danger' : ''}}">
                                <div class="m-input-icon m-input-icon--right">
                                    <input name="password" type="password" class="form-control m-input" placeholder="Enter user password">
                                    <span class="m-input-icon__icon m-input-icon__icon--right">
                                      <span>
                                      <i class="la la-key"></i>
                                      </span>
                                    </span>
                                </div>
                                <span class="form-control-feedback">
                                    {{ $errors->first('password') }}
                                </span>
                                <span class="m-form__help">
                                   Please enter user password
                                </span>
                            </div>
                        </div>

                        <div class="form-group m-form__group row">
                            <label class="col-lg-2 col-form-label">
                                Account<br />Status:
                            </label>
                            <div class="col-lg-3{{ $errors->has('status') ? ' has-danger' : ''}}">
                                <span class="m-switch m-switch--icon m-switch--success">
                                    <label>
                                        <input type="checkbox" name="status" value="1"
                                               @if(old('status', $user->status) == 1) checked @endif>
                                        <span></span>
                                    </label>
                                </span>

                                <span class="form-control-feedback">
                                    {{ $errors->first('status') }}
                                </span>
                                <span class="m-form__help">
                               Active users can login directly, Inactive users can have account but can't login unless their account is activated
                               </span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions--solid">
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-6 m--align-right">
                                <button type="button" class="ag-form-submit btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span><i class="la la-save"></i> <span> Save </span> </span>
                                </button>

                                <span class="m--margin-left-10">or <a href="{{route('list-users')}}" class="m-link m--font-bold"> Cancel </a></span>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->


    </div>
@endsection

@section('scripts')
    @parent

    <script>
        $("#form").validate({
            rules: {
                email: {
                    required: !0,
                    email: !0
                },
                name: {
                    required: !0
                },
                password: {
                    minlength: 6
                }
            }
        });

        $('.ag-form-submit').click(function(e) {
            e.preventDefault()
            $('#form').submit();
        });
        $(window).bind('beforeunload', function(e){
            // TODO: implelemt logic
            // return null
            // if($('#form').serialize()!=$('#form').data('serialize'))return true;
            // else e=null;
            // e=null
        });
    </script>
@endsection
