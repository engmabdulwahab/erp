<?php

namespace Ag\User\Controllers;

use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('ag-user::roles', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ag-user::new-role');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Role::create(['name' => $request->name]);
//        return dd($request);
        return redirect('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        //All Permissions to list all permissions available
        $permissions = Permission::with('roles')->get();
        //Permissions for the selected Role
//        $permissions = Role::findOrFail($id)->permissions;

        return view('ag-user::edit-role', compact('role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $role = Role::findOrFail($request->role_id);

        $role->name = $request->name;

        $role->update();

        return redirect('roles');
    }

    /**
     * Assign Permissions to the Role
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function assignPermissions(Request $request)
    {
        $permission = Permission::findOrFail($request->perm_id);
        $role = Role::findOrFail($request->role_id);

        //Check wether the checkbox is checked
        if (isset($request->permissions)) // 'permissions' is the name of the checkbox input
        {
            // Checkbox is checked
            $role->givePermissionTo($permission);
        }
        else
        {
            // Checkbox is not checked
            $role->revokePermissionTo($permission);
        }
        $permission = Permission::findOrFail($request->perm_id);
        $role = Role::findOrFail($request->role_id);


//        return back();
        return response()->json(array('success' => true, 'data'=>'Done'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        $role->delete();
        return redirect('roles');
    }
}
