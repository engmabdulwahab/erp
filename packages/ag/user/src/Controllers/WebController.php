<?php
/**
 * Created by PhpStorm.
 * User: Ammar AlRefai
 * Date: 12/19/18
 * Time: 4:01 PM
 */

namespace Ag\User\Controllers;

use Ag\user\src\Requests\EditUserRequest;
use Ag\user\src\Requests\NewUserRequest;
use App\Http\Controllers\Controller;
use App\User;
use Hash;

class WebController extends Controller
{
    function users() {
        return view('ag-user::users');
    }

    function createUser() {
        return view('ag-user::new-user');
    }

    function editUser(User $user) {

//        $data = [
//            'name' => old('name', '')
//        ];

        return view('ag-user::edit-user', ['user' => $user]);
    }

    function delUser(User $user) {
        // TODO: ECheck
        $name = $user->name;
        if (User::destroy($user->id)) {
            return redirect()->back()->with([
                    'message' => 'User: ' . $name . ' deleted successfully!',
                    'type' => 'success'
            ]);
        }

        return redirect()->back()->with([
            'message' => 'can\'t delete user: ' . $name . '!',
            'type' => 'danger'
        ]);
    }

    function storeUser(NewUserRequest $request) {

        $fields = ['name', 'email', 'status'];

        $atts = $request->only($fields);

        $atts['password'] = Hash::make($request->get('password'));

        $name = $request->get('name');

        $message = [
            'message' => 'User ' . $name . ' created successfully!',
            'type' => 'success'
        ];

        $user = new User($atts);
        $save = $user->saveOrFail();

        if ($save) {
            return redirect()->route('list-users')
                ->with($message);
        }

        return back()->with([
            'message' => 'Failed to create user!',
            'type' => 'danger'
        ]);
    }

    function storeEditUser(User $user, EditUserRequest $request) {

        $request->validate([
            'email' => 'required|email|unique:users,email,' . $user->id,
            'password' => 'nullable|min:6',
        ]);

        $fields   = ['name', 'email'];

        $name     = $request->get('name');
        $password = $request->get('password', null);
        $status   = $request->get('status', 0);

        $atts = $request->only($fields);
        $atts['status'] = $status;

        if (!empty($password)) {
            $atts['password'] = Hash::make($password);
        }

        $message = [
            'message' => 'User ' . $name . ' edited successfully!',
            'type' => 'success'
        ];

        $save = $user->update($atts);

        if ($save) {
            return redirect()->route('list-users')
                ->with($message);
        }

        return back()->with([
            'message' => 'Failed to edit user!',
            'type' => 'danger'
        ]);
    }
}