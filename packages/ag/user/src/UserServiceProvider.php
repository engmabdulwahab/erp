<?php
/**
 * Created by PhpStorm.
 * User: Ammar AlRefai
 * Date: 12/19/18
 * Time: 4:01 PM
 */

namespace Ag\User;

use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Routes
        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        // Views
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'ag-user');

        // Config file
        //    $this->publishes([
        //      __DIR__ . '/config.php' => config_path('user.php'),
        //    ], 'ag-user');

        // Translation
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'ag-user');

        // Migrations
        $this->loadMigrationsFrom([
        __DIR__ . '/../database/migrations'
    ]);

    }
}