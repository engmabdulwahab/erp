<?php

use App\User;
use Yajra\DataTables\Facades\DataTables;

Route::group(
    ['namespace' => 'Ag\\User\\Controllers'], function () {

    Route::group(
        ['prefix' => 'api',
            'middleware' => ['auth:api'],
        ], function () {

        //
    });


    Route::group(['middleware' => ['web']], function () { // todo; Set proper

        Route::get('users', 'WebController@users')->name('list-users');
        Route::get('users/new', 'WebController@createUser')->name('new-user');
        Route::post('users', 'WebController@storeUser')->name('new-user-store');
        Route::get('users/{user}/edit', 'WebController@editUser')->name('edit-user');
        Route::post('users/{user}', 'WebController@storeEditUser')->name('edit-user-store');
        Route::post('users/{user}/delete', 'WebController@delUser')->name('del-user');
//Roles
        Route::get('roles', 'RoleController@index')->name('list-roles');
        Route::get('roles/new', 'RoleController@create')->name('new-role');
        Route::post('roles/store', 'RoleController@store')->name('role-store');
        Route::post('roles/delete/{role_id}', 'RoleController@destroy')->name('role-delete');
        Route::get('roles/edit/{role_id}', 'RoleController@edit')->name('role-edit');
        Route::post('roles/{role_id}', 'RoleController@update')->name('edit-role');
        Route::patch('roles/{role_id}/{perm_id}', 'RoleController@assignPermissions')->name('assign-perm');


        // Roles Table
        Route::name('roles-api')->get('/roles-table', function() {

            return Datatables::of(\Spatie\Permission\Models\Role::query())->make(true);
//    return User::paginate(2);

        });

        // Users Table
        Route::name('demo-api')->get('/users-table', function() {

            return Datatables::of(User::query())->make(true);
//    return User::paginate(2);

        });
    });




});
