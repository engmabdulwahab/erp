<?php
/**
 * Created by PhpStorm.
 * User: agile
 * Date: 12/21/18
 * Time: 2:52 AM
 */

return [
    /*
     * The following skeleton will be downloaded for each new package.
     * Default: http://github.com/Jeroen-G/packager-skeleton/archive/master.zip
     */
    'skeleton' => 'https://github.com/ArabAgile/laravel-add-package-structure/archive/master.zip',

    /*
     * You can set defaults for the following placeholders.
     */
    'author_name'     => 'Ammar AlRefai',
    'author_email'    => 'eng.ammar.refai@gmail.com',
    'author_homepage' => 'http://aya.cloud',
    'license'         => 'license',
];