<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Testing.
 *
 * @author  The scaffold-interface created at 2019-06-18 06:07:58pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Testing extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'testings';

	
}
