<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Testing;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class TestingController.
 *
 * @author  The scaffold-interface created at 2019-06-18 06:07:58pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class TestingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - testing';
        $testings = Testing::paginate(6);
        return view('testing.index',compact('testings','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - testing';
        
        return view('testing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $testing = new Testing();

        
        $testing->name = $request->name;

        
        $testing->date = $request->date;

        
        
        $testing->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new testing has been created !!']);

        return redirect('testing');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - testing';

        if($request->ajax())
        {
            return URL::to('testing/'.$id);
        }

        $testing = Testing::findOrfail($id);
        return view('testing.show',compact('title','testing'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - testing';
        if($request->ajax())
        {
            return URL::to('testing/'. $id . '/edit');
        }

        
        $testing = Testing::findOrfail($id);
        return view('testing.edit',compact('title','testing'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $testing = Testing::findOrfail($id);
    	
        $testing->name = $request->name;
        
        $testing->date = $request->date;
        
        
        $testing->save();

        return redirect('testing');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/testing/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$testing = Testing::findOrfail($id);
     	$testing->delete();
        return URL::to('testing');
    }
}
