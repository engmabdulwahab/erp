@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Show')
@section('content')

<div class = 'container'>
    <h1>
        Show testing
    </h1>
    <form method = 'get' action = '{!!url("testing")!!}'>
        <button class = 'btn blue'>testing Index</button>
    </form>
    <table class = 'highlight bordered'>
        <thead>
            <th>Key</th>
            <th>Value</th>
        </thead>
        <tbody>
            <tr>
                <td>
                    <b><i>name : </i></b>
                </td>
                <td>{!!$testing->name!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>date : </i></b>
                </td>
                <td>{!!$testing->date!!}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection