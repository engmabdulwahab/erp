@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Create')
@section('content')

<div class = 'container'>
    <h1>
        Create testing
    </h1>
    <form method = 'get' action = '{!!url("testing")!!}'>
        <button class = 'btn blue'>testing Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!!url("testing")!!}'>
        <input type = 'hidden' name = '_token' value = '{{ Session::token() }}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate">
            <label for="name">name</label>
        </div>
        <div class="input-field col s6">
            <input id="date" name = "date" type="text" class="validate">
            <label for="date">date</label>
        </div>
        <button class = 'btn red' type ='submit'>Create</button>
    </form>
</div>
@endsection