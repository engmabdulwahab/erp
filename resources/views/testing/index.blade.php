@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
        testing Index
    </h1>
    <div class="row">
        <form class = 'col s3' method = 'get' action = '{!!url("testing")!!}/create'>
            <button class = 'btn red' type = 'submit'>Create New testing</button>
        </form>
    </div>
    <table>
        <thead>
            <th>name</th>
            <th>date</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($testings as $testing) 
            <tr>
                <td>{!!$testing->name!!}</td>
                <td>{!!$testing->date!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#modal1' class = 'delete btn-floating modal-trigger red' data-link = "/testing/{!!$testing->id!!}/deleteMsg" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/testing/{!!$testing->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/testing/{!!$testing->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
            @endforeach 
        </tbody>
    </table>
    {!! $testings->render() !!}

</div>
@endsection