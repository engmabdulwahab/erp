@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Edit')
@section('content')

<div class = 'container'>
    <h1>
        Edit testing
    </h1>
    <form method = 'get' action = '{!!url("testing")!!}'>
        <button class = 'btn blue'>testing Index</button>
    </form>
    <br>
    <form method = 'POST' action = '{!! url("testing")!!}/{!!$testing->
        id!!}/update'> 
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="input-field col s6">
            <input id="name" name = "name" type="text" class="validate" value="{!!$testing->
            name!!}"> 
            <label for="name">name</label>
        </div>
        <div class="input-field col s6">
            <input id="date" name = "date" type="text" class="validate" value="{!!$testing->
            date!!}"> 
            <label for="date">date</label>
        </div>
        <button class = 'btn red' type ='submit'>Update</button>
    </form>
</div>
@endsection