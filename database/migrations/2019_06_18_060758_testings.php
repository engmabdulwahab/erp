<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Testings.
 *
 * @author  The scaffold-interface created at 2019-06-18 06:07:58pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Testings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('testings',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('name');
        
        $table->date('date');
        
        /**
         * Foreignkeys section
         */
        
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('testings');
    }
}
