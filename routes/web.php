<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//testing Routes
Route::group(['middleware'=> 'web','auth'],function(){
    //Roles Permissions
    Route::get('scaffold-rolepermissions', '\App\Http\Controllers\ScaffoldInterface\RolePermissionsController@index');
    Route::get('scaffold-rolepermissions/edit/{role_id}', '\App\Http\Controllers\ScaffoldInterface\RolePermissionsController@edit');
    Route::post('scaffold-rolepermissions/update', '\App\Http\Controllers\ScaffoldInterface\RolePermissionsController@update');
    Route::get('scaffold-rolepermissions/create', '\App\Http\Controllers\ScaffoldInterface\RolePermissionsController@create');
    Route::post('scaffold-rolepermissions/store', '\App\Http\Controllers\ScaffoldInterface\RolePermissionsController@store');
    Route::get('scaffold-rolepermissions/delete/{role_id}', '\App\Http\Controllers\ScaffoldInterface\RolePermissionsController@destroy');

});
